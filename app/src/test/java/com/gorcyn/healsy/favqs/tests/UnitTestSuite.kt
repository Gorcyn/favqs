package com.gorcyn.healsy.favqs.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.healsy.favqs.data.source.QuoteRepositoryTest
import com.gorcyn.healsy.favqs.data.source.UserRepositoryTest

@RunWith(Suite::class)

@Suite.SuiteClasses(
        QuoteRepositoryTest::class,
        UserRepositoryTest::class
)
class UnitTestSuite
