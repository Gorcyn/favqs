package com.gorcyn.healsy.favqs.data.source

import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when`
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

import org.hamcrest.CoreMatchers.`is`

import io.reactivex.observers.TestObserver
import io.reactivex.Single

import com.nhaarman.mockito_kotlin.any

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.local.QuoteLocalDataSource
import com.gorcyn.healsy.favqs.data.source.remote.QuoteRemoteDataSource
import com.gorcyn.healsy.favqs.tests.RxTest

class QuoteRepositoryTest: RxTest() {

    companion object {
        private val USER = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)
        private val QUOTE_LIST = listOf(
                Quote(1, "AUTHOR 1", "BODY 1", listOf("TAG 1.1", "TAG 1.2")),
                Quote(2, "AUTHOR 2", "BODY 2", listOf("TAG 2.1", "TAG 2.2")),
                Quote(3, "AUTHOR 3", "BODY 3", listOf("TAG 3.1", "TAG 3.2"))
        )
    }

    @Mock
    private lateinit var localDataSource: QuoteLocalDataSource
    @Mock
    private lateinit var remoteDataSource: QuoteRemoteDataSource

    private lateinit var quoteRepository: QuoteRepository
    private lateinit var testObserver: TestObserver<List<Quote>>

    @Before
    fun setupQuoteRepository() {
        MockitoAnnotations.initMocks(this)

        quoteRepository = QuoteRepository.getInstance(localDataSource, remoteDataSource)
        testObserver = TestObserver()
    }

    @After
    fun destroyRepositoryInstance() {
        QuoteRepository.clearInstance()
    }

    @Test
    fun getQuoteList_repositoryCachesAfterFirstSubscription_whenQuoteListAvailableInLocalStorage() {
        // Given that the local data source has data available
        setQuoteListNotAvailable(localDataSource, USER, 1)
        // And the remote data source does not have any data available
        setQuoteListAvailable(remoteDataSource, QUOTE_LIST, USER, 1)

        // When two subscriptions are set
        val testObserver1 = TestObserver<List<Quote>>()
        val testObserver2 = TestObserver<List<Quote>>()

        quoteRepository.cacheIsDirty = true
        quoteRepository.getList(USER, 1).subscribe(testObserver1)
        quoteRepository.getList(USER, 1).subscribe(testObserver2)

        // Then quotes were only requested once from remote and local sources
        verify<QuoteDataSource>(remoteDataSource).getList(USER, 1)
        verify<QuoteDataSource>(localDataSource, never()).getList(USER, 1)
        //
        assertFalse(quoteRepository.cacheIsDirty)
        testObserver1.assertValue(QUOTE_LIST)
        testObserver2.assertValue(QUOTE_LIST)
    }

    @Test
    fun getQuoteList_repositoryCachesAfterFirstSubscription_whenQuoteListAvailableInRemoteStorage() {
        // Given that the local data source has data available
        setQuoteListAvailable(remoteDataSource, QUOTE_LIST, USER, 1)
        // And the remote data source does not have any data available
        setQuoteListNotAvailable(localDataSource, USER, 1)

        // When two subscriptions are set
        val testObserver1 = TestObserver<List<Quote>>()
        quoteRepository.getList(USER, 1).subscribe(testObserver1)

        val testObserver2 = TestObserver<List<Quote>>()
        quoteRepository.getList(USER, 1).subscribe(testObserver2)

        // Then quotes were only requested once from remote and local sources
        verify<QuoteDataSource>(remoteDataSource).getList(USER, 1)
        verify<QuoteDataSource>(localDataSource, never()).getList(USER, 1)
        //
        assertFalse(quoteRepository.cacheIsDirty)
        testObserver1.assertValue(QUOTE_LIST)
        testObserver2.assertValue(QUOTE_LIST)
    }

    @Test
    fun getQuoteList_requestsQuoteListFromLocalDataSource() {
        // Given that the local data source has data available
        setQuoteListAvailable(localDataSource, QUOTE_LIST, USER, 1)
        // And the remote data source does not have any data available
        setQuoteListNotAvailable(remoteDataSource, USER, 1)

        // When quotes are requested from the quote repository
        quoteRepository.cacheIsDirty = false
        quoteRepository.getList(USER, 1).subscribe(testObserver)

        // Then quotes are loaded from the local data source
        verify<QuoteDataSource>(localDataSource).getList(USER, 1)
        testObserver.assertValue(QUOTE_LIST)
    }

    @Test(expected = NotImplementedError::class)
    fun saveQuote_savesQuoteToServiceAPI() {
        // Given a stub quote
        val newQuote = Quote(1, "AUTHOR", "BODY", listOf("TAG"))

        // When a quote is saved to the quote repository
        quoteRepository.save(newQuote).subscribe(testObserver)
    }

    @Test
    fun getQuoteListWithDirtyCache_quoteListIsRetrievedFromRemote() {
        // Given that the remote data source has data available
        setQuoteListAvailable(remoteDataSource, QUOTE_LIST, USER, 1)

        // When calling fetchQuoteList in the repository with dirty cache
        quoteRepository.refreshList()
        quoteRepository.getList(USER, 1).subscribe(testObserver)

        // Verify the quotes from the remote data source are returned, not the local
        verify<QuoteDataSource>(localDataSource, never()).getList(USER, 1)
        verify<QuoteDataSource>(remoteDataSource).getList(USER, 1)
        testObserver.assertValue(QUOTE_LIST)
    }

    @Test
    fun getQuoteListWithLocalDataSourceUnavailable_quoteListIsRetrievedFromRemote() {
        // Given that the local data source has no data available
        setQuoteListNotAvailable(localDataSource, USER, 1)
        // And the remote data source has data available
        setQuoteListAvailable(remoteDataSource, QUOTE_LIST, USER, 1)

        // When calling fetchQuoteList in the repository
        quoteRepository.getList(USER, 1).subscribe(testObserver)

        // Verify the quotes from the remote data source are returned
        verify<QuoteDataSource>(remoteDataSource).getList(USER, 1)
        testObserver.assertValue(QUOTE_LIST)
    }

    @Test
    fun getQuoteListWithBothDataSourcesUnavailable_firesOnDataUnavailable() {
        // Given that the local data source has no data available
        setQuoteListNotAvailable(localDataSource, USER, 1)
        // And the remote data source has no data available
        setQuoteListNotAvailable(remoteDataSource, USER, 1)

        // When calling fetchQuoteList in the repository
        quoteRepository.getList(USER, 1).subscribe(testObserver)

        // Verify no data is returned
        testObserver.assertValue(listOf())
    }

    @Test
    fun getQuoteList_refreshesLocalDataSource() {
        // Given that the remote data source has data available
        setQuoteListAvailable(remoteDataSource, QUOTE_LIST, USER, 1)

        // Mark cache as dirty to force a reload of data from remote data source.
        quoteRepository.refreshList()

        // When calling fetchQuoteList in the repository
        quoteRepository.getList(USER, 1).subscribe(testObserver)

        // Verify that the data fetched from the remote data source was saved in local.
        verify<QuoteDataSource>(localDataSource, times(QUOTE_LIST.size)).save(any())
        testObserver.assertValue(QUOTE_LIST)
    }

    private fun setQuoteListNotAvailable(dataSource: QuoteDataSource, user: User, page: Int) {
        `when`(dataSource.getList(user, page)).thenReturn(Single.just(emptyList()))
    }

    private fun setQuoteListAvailable(dataSource: QuoteDataSource, quoteList: List<Quote>, user: User, page: Int) {
        `when`(dataSource.getList(user, page)).thenReturn(
                Single.just(quoteList)
        )
    }
}
