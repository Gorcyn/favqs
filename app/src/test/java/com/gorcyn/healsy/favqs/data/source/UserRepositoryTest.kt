package com.gorcyn.healsy.favqs.data.source

import org.junit.After
import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when`
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

import io.reactivex.observers.TestObserver
import io.reactivex.Maybe

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq

import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.local.UserLocalDataSource
import com.gorcyn.healsy.favqs.data.source.remote.UserRemoteDataSource
import com.gorcyn.healsy.favqs.tests.RxTest

class UserRepositoryTest: RxTest() {

    companion object {
        private val USER = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)
        private const val PASSWORD = "password"
    }

    @Mock
    private lateinit var localDataSource: UserLocalDataSource
    @Mock
    private lateinit var remoteDataSource: UserRemoteDataSource

    private lateinit var userRepository: UserRepository
    private lateinit var testObserver: TestObserver<User>

    @Before
    fun setupUserRepository() {
        MockitoAnnotations.initMocks(this)

        userRepository = UserRepository.getInstance(localDataSource, remoteDataSource)
        testObserver = TestObserver()
    }

    @After
    fun destroyRepositoryInstance() {
        UserRepository.clearInstance()
    }

    @Test
    fun login_repositoryCallsRemoteButNotLocal() {
        setUserAvailable(localDataSource, USER, PASSWORD)
        setUserAvailable(remoteDataSource, USER, PASSWORD)

        userRepository.login(USER.login, PASSWORD).subscribe(testObserver)

        verify<UserDataSource>(localDataSource).login(USER.login, PASSWORD)
        verify<UserDataSource>(remoteDataSource, never()).login(USER.login, PASSWORD)
        testObserver.assertValue(USER)
    }

    @Test
    fun login_repositoryCallsRemoteThenLocalToSave() {
        setUserAvailable(localDataSource, USER, PASSWORD)
        setUserAvailable(remoteDataSource, USER, PASSWORD)

        userRepository.login(USER.login, PASSWORD).subscribe(testObserver)

        verify<UserDataSource>(remoteDataSource, never()).login(USER.login, PASSWORD)
        verify<UserDataSource>(localDataSource, never()).save(USER)
        testObserver.assertValue(USER)
    }

    @Test
    fun login_repositoryCallsRemoteThatFails() {
        setUserNotAvailable(remoteDataSource, USER, PASSWORD)
        setUserNotAvailable(localDataSource, USER, PASSWORD)

        userRepository.login(USER.login, PASSWORD).subscribe(testObserver)

        testObserver.assertNoValues()
        testObserver.assertError(NoSuchElementException::class.java)
    }

    @Test
    fun getDetails_repositoryCallsRemoteButNotLocal() {
        setUserAvailable(remoteDataSource, USER, PASSWORD)

        userRepository.getDetails(USER).subscribe(testObserver)

        verify<UserDataSource>(remoteDataSource).getDetails(USER)
        verify<UserDataSource>(localDataSource, never()).getDetails(USER)
        testObserver.assertValue(USER)
    }

    @Test
    fun getDetails_repositoryCallsRemoteThenLocalToSave() {
        setUserAvailable(remoteDataSource, USER, PASSWORD)

        userRepository.getDetails(USER).subscribe(testObserver)

        verify<UserDataSource>(remoteDataSource).getDetails(USER)
        verify<UserDataSource>(localDataSource).save(USER)
        verify<UserDataSource>(localDataSource, times(1)).save(any())
        testObserver.assertValue(USER)
    }

    @Test
    fun getDetails_repositoryCallsRemoteThatFails() {
        setUserNotAvailable(localDataSource, USER, PASSWORD)
        setUserNotAvailable(remoteDataSource, USER, PASSWORD)

        userRepository.getDetails(USER).subscribe(testObserver)

        testObserver.assertNoValues()
        testObserver.assertError(NoSuchElementException::class.java)
    }

    private fun setUserNotAvailable(dataSource: UserDataSource, user: User, password: String) {
        `when`(dataSource.login(eq(user.login), eq(password))).thenReturn(
                Maybe.error(NoSuchElementException("The MaybeSource is empty"))
        )
        `when`(dataSource.getDetails(eq(user))).thenReturn(
                Maybe.error(NoSuchElementException("The MaybeSource is empty"))
        )
    }

    private fun setUserAvailable(dataSource: UserDataSource, user: User, password: String) {
        `when`(dataSource.login(eq(user.login), eq(password))).thenReturn(
                Maybe.just(user)
        )
        `when`(dataSource.getDetails(eq(user))).thenReturn(
                Maybe.just(user)
        )
    }
}
