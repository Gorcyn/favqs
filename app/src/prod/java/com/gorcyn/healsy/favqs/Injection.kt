package com.gorcyn.healsy.favqs

import android.content.Context
import android.net.ConnectivityManager
import android.preference.PreferenceManager

import com.gorcyn.healsy.favqs.data.source.QuoteDataSource
import com.gorcyn.healsy.favqs.data.source.QuoteRepository
import com.gorcyn.healsy.favqs.data.source.UserDataSource
import com.gorcyn.healsy.favqs.data.source.UserRepository
import com.gorcyn.healsy.favqs.data.source.local.AppDatabase
import com.gorcyn.healsy.favqs.data.source.local.QuoteLocalDataSource
import com.gorcyn.healsy.favqs.data.source.local.UserLocalDataSource
import com.gorcyn.healsy.favqs.data.source.remote.QuoteRemoteDataSource
import com.gorcyn.healsy.favqs.data.source.remote.UserRemoteDataSource
import com.gorcyn.healsy.favqs.data.source.remote.QuoteService
import com.gorcyn.healsy.favqs.data.source.remote.UserService
import com.gorcyn.healsy.favqs.util.schedulers.BaseSchedulerProvider
import com.gorcyn.healsy.favqs.util.schedulers.SchedulerProvider

class Injection {

    companion object {
        fun provideUserDataSource(context: Context): UserDataSource {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return UserRepository.getInstance(
                    UserLocalDataSource.getInstance(PreferenceManager.getDefaultSharedPreferences(context)),
                    UserRemoteDataSource.getInstance(connectivityManager, UserService.instance)
            )
        }

        fun provideQuoteDataSource(context: Context): QuoteDataSource {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return QuoteRepository.getInstance(
                    QuoteLocalDataSource.getInstance(AppDatabase.getInstance(context)),
                    QuoteRemoteDataSource.getInstance(connectivityManager, QuoteService.instance)
            )
        }

        fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider
    }
}
