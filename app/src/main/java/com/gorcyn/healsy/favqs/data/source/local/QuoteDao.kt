package com.gorcyn.healsy.favqs.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote

@Dao
interface QuoteDao {

    @Query("SELECT * FROM quote LIMIT 25 OFFSET ((:page - 1) * 25)")
    fun list(page: Int): Single<List<Quote>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(quote: Quote)

    @Query("DELETE FROM quote")
    fun deleteAll()
}
