package com.gorcyn.healsy.favqs.data.source.exception

import kotlin.Exception

class NetworkUnavailableExceptionn(
        message: String?,
        cause: Throwable? = null
): Exception(message, cause)
