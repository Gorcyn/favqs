package com.gorcyn.healsy.favqs.data.source.remote

import android.net.ConnectivityManager

import io.reactivex.Completable
import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.QuoteDataSource
import com.gorcyn.healsy.favqs.data.source.exception.NetworkUnavailableExceptionn
import com.gorcyn.healsy.favqs.util.SingletonHolderDoubleArg

open class QuoteRemoteDataSource(
        private val connectivityManager: ConnectivityManager,
        private val quoteService: QuoteService
): QuoteDataSource {
    companion object : SingletonHolderDoubleArg<QuoteRemoteDataSource, ConnectivityManager, QuoteService>(::QuoteRemoteDataSource)

    override fun getList(user: User, page: Int): Single<List<Quote>> {
        connectivityManager.activeNetworkInfo?.let {
            if (!it.isConnected) {
                return Single.error(NetworkUnavailableExceptionn("Network is unavailable"))
            }
        }
        return quoteService.getList(user.userToken, user.login, page)
                .map { it.quotes }
    }

    override fun save(quote: Quote): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun refreshList(clear: Boolean) {
        // Implemented in repository
    }
}
