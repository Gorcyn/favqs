package com.gorcyn.healsy.favqs.ui.profile

import android.arch.lifecycle.ViewModel

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.UserDataSource
import com.gorcyn.healsy.favqs.util.schedulers.BaseSchedulerProvider

class ProfileViewModel(
        private val repository: UserDataSource,
        private val scheduler: BaseSchedulerProvider
): ViewModel() {

    private val disposables = CompositeDisposable()

    val isLoading: Relay<Boolean> = PublishRelay.create()
    val user: Relay<User> = PublishRelay.create()
    val error: Relay<Throwable> = PublishRelay.create()

    fun getProfile(user: User) {
        isLoading.accept(true)
        disposables += repository.getDetails(user)
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribe({
                    isLoading.accept(false)
                    this.user.accept(it)
                }, {
                    isLoading.accept(false)
                    error.accept(it)
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
