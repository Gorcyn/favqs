package com.gorcyn.healsy.favqs.data

import android.os.Parcelable

import com.google.gson.annotations.SerializedName

import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
        @SerializedName("login")
        val login: String,

        @SerializedName("User-Token")
        val userToken: String,

        @SerializedName("pic_url")
        val picUrl: String?,

        @SerializedName("public_favorites_count")
        val favoritesCount: Int = 0,

        @SerializedName("error_code")
        val errorCode: Int? = null
): Parcelable
