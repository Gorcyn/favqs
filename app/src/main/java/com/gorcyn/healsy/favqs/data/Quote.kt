package com.gorcyn.healsy.favqs.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable

import com.google.gson.annotations.SerializedName

import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "quote")
data class Quote(
        @PrimaryKey
        @SerializedName("id")
        var id: Long,

        @ColumnInfo(name = "author")
        @SerializedName("author")
        var author: String,

        @ColumnInfo(name = "body")
        @SerializedName("body")
        var body: String,

        @ColumnInfo(name = "tags")
        @SerializedName("tags")
        var tags: List<String>
): Parcelable
