package com.gorcyn.healsy.favqs.data.source.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote

data class QuoteList(
        val page: Int,
        val last_page: Boolean,
        val quotes: List<Quote>
)

interface QuoteService {
    companion object {
        val instance: QuoteService by lazy {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            Retrofit.Builder()
                    .client(client)
                    .baseUrl("https://favqs.com/api/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(QuoteService::class.java)
        }
    }

    @GET("quotes/?type=user")
    @Headers("Authorization: Token token=\"18bc3caa9c8cf5fa636db6ee15eb5a28\"")
    fun getList(@Header("User-Token") userToken: String, @Query("filter") filter: String, @Query("page") page: Int = 1): Single<QuoteList>
}
