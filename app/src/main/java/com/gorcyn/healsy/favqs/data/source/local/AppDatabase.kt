package com.gorcyn.healsy.favqs.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.util.SingletonHolderSingleArg

@Database(entities = [Quote::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {

    companion object : SingletonHolderSingleArg<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext, AppDatabase::class.java, "app.db")
                .build()
    })

    abstract fun quoteDao(): QuoteDao
}
