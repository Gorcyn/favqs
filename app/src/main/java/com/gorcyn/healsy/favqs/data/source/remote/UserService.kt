package com.gorcyn.healsy.favqs.data.source.remote

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.POST
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import io.reactivex.Maybe

import com.gorcyn.healsy.favqs.data.User

data class Credentials(val user: CredentialsUser) {
    data class CredentialsUser(val login: String, val password: String)
}

interface UserService {
    companion object {
        val instance: UserService by lazy {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            Retrofit.Builder()
                    .client(client)
                    .baseUrl("https://favqs.com/api/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(UserService::class.java)
        }
    }

    @POST("session")
    @Headers("Authorization: Token token=\"18bc3caa9c8cf5fa636db6ee15eb5a28\"")
    fun login(@Body credentials: Credentials): Maybe<User>

    @GET("users/{user}")
    @Headers("Authorization: Token token=\"18bc3caa9c8cf5fa636db6ee15eb5a28\"")
    fun getDetails(@Path("user") user: String, @Header("User-Token") userToken: String): Maybe<User>
}
