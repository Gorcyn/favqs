package com.gorcyn.healsy.favqs.ui.profile

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import com.jakewharton.rxbinding2.view.clicks

import com.bumptech.glide.Glide

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.extensions.addFragment
import com.gorcyn.healsy.favqs.extensions.snack
import com.gorcyn.healsy.favqs.ui.quoteList.QuoteListFragment
import com.gorcyn.healsy.favqs.util.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_profile.profileProgressBar
import kotlinx.android.synthetic.main.fragment_profile.profileThumbnail
import kotlinx.android.synthetic.main.fragment_profile.profileTitle
import kotlinx.android.synthetic.main.fragment_profile.profileFavoritesButton

class ProfileFragment: Fragment() {

    companion object {
        private const val USER = "USER"
        const val TAG = "ProfileFragment"

        fun newInstance(user: User): ProfileFragment {
            val bundle = Bundle()
            bundle.putParcelable(USER, user)
            val fragment = ProfileFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private val disposables = CompositeDisposable()

    private val user: User by lazy {
        arguments!![USER] as User
    }

    private val viewModel: ProfileViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory.getInstance(context!!))
                .get(ProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onResume() {
        super.onResume()

        // Observe view model
        disposables += viewModel.isLoading
                .subscribe({ profileProgressBar.isRefreshing = it }, {})
        disposables += viewModel.user
                .subscribe({
                    profileTitle.text = it.login
                    profileFavoritesButton.text = context!!.getString(R.string.profile_fav_button, it.favoritesCount)
                    it.picUrl?.let{ url ->
                        Glide.with(context!!)
                                .load(url)
                                .into(profileThumbnail)
                    }
                }, {})
        disposables += viewModel.error
                .subscribe({ snack(it.localizedMessage) }, {})

        // Observe user interactions
        disposables += profileProgressBar.refreshes().forEach { viewModel.getProfile(user) }
        disposables += profileFavoritesButton.clicks().forEach {
            addFragment(QuoteListFragment.newInstance(user), R.id.container, QuoteListFragment.TAG)
        }

        // Load right now
        viewModel.getProfile(user)
    }

    override fun onPause() {
        super.onPause()
        disposables.clear()
    }
}
