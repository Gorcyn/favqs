package com.gorcyn.healsy.favqs.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context

import com.gorcyn.healsy.favqs.Injection
import com.gorcyn.healsy.favqs.ui.login.LoginViewModel
import com.gorcyn.healsy.favqs.ui.profile.ProfileViewModel
import com.gorcyn.healsy.favqs.ui.quoteList.QuoteListViewModel

class ViewModelFactory private constructor(
        private val applicationContext: Context
): ViewModelProvider.Factory {
    companion object: SingletonHolderSingleArg<ViewModelFactory, Context>(::ViewModelFactory)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == LoginViewModel::class.java) {
            return LoginViewModel(
                    Injection.provideUserDataSource(applicationContext),
                    Injection.provideSchedulerProvider()
            ) as T
        }
        if (modelClass == ProfileViewModel::class.java) {
            return ProfileViewModel(
                    Injection.provideUserDataSource(applicationContext),
                    Injection.provideSchedulerProvider()
            ) as T
        }
        if (modelClass == QuoteListViewModel::class.java) {
            return QuoteListViewModel(
                    Injection.provideQuoteDataSource(applicationContext),
                    Injection.provideSchedulerProvider()
            ) as T
        }
        throw IllegalArgumentException("Unknown model class $modelClass")
    }
}
