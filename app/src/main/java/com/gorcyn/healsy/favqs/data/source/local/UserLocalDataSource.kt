package com.gorcyn.healsy.favqs.data.source.local

import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting

import io.reactivex.Completable
import io.reactivex.Maybe

import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.UserDataSource
import com.gorcyn.healsy.favqs.util.SingletonHolderSingleArg

open class UserLocalDataSource(
        private val sharedPreferences: SharedPreferences
): UserDataSource {
    companion object : SingletonHolderSingleArg<UserLocalDataSource, SharedPreferences>(::UserLocalDataSource) {
        private const val LOGIN = "LOGIN"
        private const val USER_TOKEN = "USER_TOKEN"
        private const val PIC_URL = "PIC_URL"
        private const val FAV_COUNT = "FAV_COUNT"
    }

    override fun login(login: String, password: String): Maybe<User> {
        val oldLogin = sharedPreferences.getString(LOGIN, null)

        if (oldLogin != login) {
            return Maybe.empty()
        }
        val oldToken = sharedPreferences.getString(USER_TOKEN, null)
        return if (oldToken == null) {
            Maybe.empty()
        } else {
            Maybe.just(User(oldLogin, oldToken,
                    sharedPreferences.getString(PIC_URL, null), sharedPreferences.getInt(FAV_COUNT, 0)
            ))
        }
    }

    override fun getDetails(user: User): Maybe<User> {
        val login = sharedPreferences.getString(LOGIN, null)
        val token = sharedPreferences.getString(USER_TOKEN, null)
        if (login == null || token == null) {
            return Maybe.empty()
        }
        return Maybe.just(User(login, token,
                sharedPreferences.getString(PIC_URL, null), sharedPreferences.getInt(FAV_COUNT, 0)
        ))
    }

    override fun save(user: User): Completable {
        sharedPreferences.edit()
                .putString(LOGIN, user.login)
                .putString(USER_TOKEN, user.userToken)
                .putString(PIC_URL, user.picUrl)
                .putInt(FAV_COUNT, user.favoritesCount)
                .apply()
        return Completable.complete()
    }

    @VisibleForTesting
    fun delete(): Completable {
        sharedPreferences.edit().clear().apply()
        return Completable.complete()
    }
}
