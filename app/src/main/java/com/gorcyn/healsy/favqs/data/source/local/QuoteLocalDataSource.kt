package com.gorcyn.healsy.favqs.data.source.local

import android.support.annotation.VisibleForTesting

import io.reactivex.Completable
import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.QuoteDataSource
import com.gorcyn.healsy.favqs.util.SingletonHolderSingleArg

open class QuoteLocalDataSource(
        private val appDatabase: AppDatabase
): QuoteDataSource {
    companion object : SingletonHolderSingleArg<QuoteLocalDataSource, AppDatabase>(::QuoteLocalDataSource)

    override fun getList(user: User, page: Int): Single<List<Quote>> {
        return appDatabase.quoteDao().list(page)
    }

    override fun save(quote: Quote): Completable {
        appDatabase.quoteDao().save(quote)
        return Completable.complete()
    }

    override fun refreshList(clear: Boolean) {
        // Implemented in repository
    }

    @VisibleForTesting
    fun deleteAll(): Completable {
        appDatabase.quoteDao().deleteAll()
        return Completable.complete()
    }
}
