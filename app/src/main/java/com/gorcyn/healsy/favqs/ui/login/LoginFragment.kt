package com.gorcyn.healsy.favqs.ui.login

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import com.jakewharton.rxbinding2.view.clicks

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.extensions.addFragment
import com.gorcyn.healsy.favqs.extensions.snack
import com.gorcyn.healsy.favqs.ui.profile.ProfileFragment
import com.gorcyn.healsy.favqs.util.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_login.loginProgressBar
import kotlinx.android.synthetic.main.fragment_login.loginLoginField
import kotlinx.android.synthetic.main.fragment_login.loginPasswordField
import kotlinx.android.synthetic.main.fragment_login.loginButton

class LoginFragment: Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private val disposables = CompositeDisposable()

    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory.getInstance(context!!))
                .get(LoginViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onResume() {
        super.onResume()

        // Disable pull to refresh
        loginProgressBar.isEnabled = false

        // Observe view model
        disposables += viewModel.isLoading.subscribe({
            loginProgressBar.isEnabled = it
            loginProgressBar.isRefreshing = it
        }, {})
        disposables += viewModel.user.subscribe({
            addFragment(ProfileFragment.newInstance(it), R.id.container, ProfileFragment.TAG)
        }, {})
        disposables += viewModel.error.subscribe({ snack(it.localizedMessage) }, {})

        // Observe user interactions
        disposables += loginButton.clicks()
                .filter { !(loginLoginField.text?.toString()?.isEmpty() ?: false) }
                .filter { !(loginPasswordField.text?.toString()?.isEmpty() ?: false) }
                .forEach {
                    viewModel.login(loginLoginField.text!!.toString(), loginPasswordField.text!!.toString())
                }
    }

    override fun onPause() {
        super.onPause()
        disposables.clear()
    }
}
