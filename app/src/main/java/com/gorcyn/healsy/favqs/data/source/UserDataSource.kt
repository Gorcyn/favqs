package com.gorcyn.healsy.favqs.data.source

import io.reactivex.Completable
import io.reactivex.Maybe

import com.gorcyn.healsy.favqs.data.User

interface UserDataSource {

    fun login(login: String, password: String): Maybe<User>
    fun getDetails(user: User): Maybe<User>
    fun save(user: User): Completable
}
