package com.gorcyn.healsy.favqs.data.source.remote

import android.net.ConnectivityManager

import io.reactivex.Completable
import io.reactivex.Maybe

import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.UserDataSource
import com.gorcyn.healsy.favqs.data.source.exception.NetworkUnavailableExceptionn
import com.gorcyn.healsy.favqs.util.SingletonHolderDoubleArg

open class UserRemoteDataSource(
        private val connectivityManager: ConnectivityManager,
        private val service: UserService
): UserDataSource {
    companion object : SingletonHolderDoubleArg<UserRemoteDataSource, ConnectivityManager, UserService>(::UserRemoteDataSource)

    override fun login(login: String, password: String): Maybe<User> {
        connectivityManager.activeNetworkInfo?.let {
            if (!it.isConnected) {
                return Maybe.error(NetworkUnavailableExceptionn("Network is unavailable"))
            }
        }
        return service.login(Credentials(Credentials.CredentialsUser(login, password)))
    }

    override fun getDetails(user: User): Maybe<User> {
        connectivityManager.activeNetworkInfo?.let {
            if (!it.isConnected) {
                return Maybe.error(NetworkUnavailableExceptionn("Network is unavailable"))
            }
        }
        return service.getDetails(user.login, user.userToken)
    }

    override fun save(user: User): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
