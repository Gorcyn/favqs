package com.gorcyn.healsy.favqs.data.source

import android.support.annotation.VisibleForTesting

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.util.SingletonHolderDoubleArg

open class QuoteRepository(
        private val localDataSource: QuoteDataSource,
        private val remoteDataSource: QuoteDataSource
): QuoteDataSource {

    @VisibleForTesting
    var cachedQuoteList: MutableMap<Long, Quote>? = null
    @VisibleForTesting
    var cacheIsDirty = true

    companion object : SingletonHolderDoubleArg<QuoteRepository, QuoteDataSource, QuoteDataSource>(::QuoteRepository)

    override fun refreshList(clear: Boolean) {
        cacheIsDirty = true
        if (clear) {
            cachedQuoteList = LinkedHashMap()
        }
    }

    override fun getList(user: User, page: Int): Single<List<Quote>> {
        if (cachedQuoteList != null && !cacheIsDirty) {
            return Single.just(cachedQuoteList!!.values.toList())
        } else if (cachedQuoteList == null) {
            cachedQuoteList = LinkedHashMap()
        }
        return if (cacheIsDirty) {
            getAndSaveRemoteQuoteList(user, page)
                    .onErrorResumeNext { getAndCacheLocalQuoteList(user, page) }
        } else {
            getAndCacheLocalQuoteList(user, page)
        }
    }

    override fun save(quote: Quote): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun getAndCacheLocalQuoteList(user: User, page: Int): Single<List<Quote>> {
        return localDataSource.getList(user, page)
                .flatMap {
                    Observable.fromIterable(it)
                            .doOnNext { quote -> cachedQuoteList!![quote.id] = quote }
                            .toList()
                }
                .map { cachedQuoteList!!.values.toList() }
    }

    private fun getAndSaveRemoteQuoteList(user: User, page: Int): Single<List<Quote>> {
        return remoteDataSource.getList(user, page)
                .flatMap { list ->
                    Observable.fromIterable(list)
                            .doOnNext {
                                localDataSource.save(it)
                                cachedQuoteList!![it.id] = it
                            }
                            .toList()
                }
                .map { cachedQuoteList!!.values.toList() }
                .doOnSuccess { cacheIsDirty = false }
    }
}
