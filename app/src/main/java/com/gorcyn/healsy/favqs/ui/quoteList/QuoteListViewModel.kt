package com.gorcyn.healsy.favqs.ui.quoteList

import android.arch.lifecycle.ViewModel

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.QuoteDataSource
import com.gorcyn.healsy.favqs.util.schedulers.BaseSchedulerProvider

class QuoteListViewModel(
        private val repository: QuoteDataSource,
        private val scheduler: BaseSchedulerProvider
): ViewModel() {

    private val disposables = CompositeDisposable()
    private var page: Int = 1

    val isLoading: Relay<Boolean> = PublishRelay.create()
    val quoteList: Relay<List<Quote>> = PublishRelay.create()
    val error: Relay<Throwable> = PublishRelay.create()
    val hasMore: Relay<Boolean> = PublishRelay.create()

    fun getList(user: User, forceUpdate: Boolean = false) {
        page = 1

        isLoading.accept(true)
        if (forceUpdate) {
            repository.refreshList(true)
        }
        disposables += repository.getList(user, page)
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribe({
                    isLoading.accept(false)
                    quoteList.accept(it)
                    hasMore.accept(it.size == 25)
                }, {
                    isLoading.accept(false)
                    error.accept(it)
                })
    }

    fun getNextList(user: User) {
        val pageLoaded = page + 1

        isLoading.accept(true)
        repository.refreshList()
        disposables += repository.getList(user, pageLoaded)
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribe({
                    page = pageLoaded
                    isLoading.accept(false)
                    quoteList.accept(it)
                    hasMore.accept(it.size == 25)
                }, {
                    isLoading.accept(false)
                    error.accept(it)
                })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
        repository.refreshList(true)
    }
}
