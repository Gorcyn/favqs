package com.gorcyn.healsy.favqs.data.source

import io.reactivex.Completable
import io.reactivex.Single

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User

interface QuoteDataSource {

    fun getList(user: User, page: Int): Single<List<Quote>>
    fun save(quote: Quote): Completable
    fun refreshList(clear: Boolean = false)
}
