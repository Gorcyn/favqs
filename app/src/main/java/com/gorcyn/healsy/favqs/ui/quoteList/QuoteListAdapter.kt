package com.gorcyn.healsy.favqs.ui.quoteList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.jakewharton.rxbinding2.view.clicks

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.data.Quote

import kotlinx.android.synthetic.main.item_quote.view.quoteListItemQuoteAuthor
import kotlinx.android.synthetic.main.item_quote.view.quoteListItemQuoteBody
import kotlinx.android.synthetic.main.item_quote.view.quoteListItemQuoteTags

open class QuoteListAdapter(
        private val onMoreClickListener: () -> Unit
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    enum class ViewType { QUOTE, MORE }

    var quoteList: List<Quote> = emptyList()
    var hasMore: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return if (hasMore && quoteList.size == position) ViewType.MORE.ordinal else ViewType.QUOTE.ordinal
    }

    override fun getItemCount(): Int {
        return quoteList.size + if (hasMore) 1 else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            ViewType.QUOTE.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_quote, parent, false)
                return QuoteViewHolder(view)
            }
            ViewType.MORE.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_quote_more, parent, false)
                return MoreViewHolder(view)
            }
        }
        throw IllegalArgumentException("Unknown view type $viewType")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is QuoteViewHolder -> {
                holder.body.text = quoteList[position].body
                holder.author.text = quoteList[position].author
                holder.tags.text = quoteList[position].tags.joinToString(",  ")
            }
            is MoreViewHolder -> {
                holder.itemView.clicks().forEach { onMoreClickListener() }
            }
        }
    }

    class QuoteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val body: TextView = itemView.quoteListItemQuoteBody
        val author: TextView = itemView.quoteListItemQuoteAuthor
        val tags: TextView = itemView.quoteListItemQuoteTags
    }

    class MoreViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}
