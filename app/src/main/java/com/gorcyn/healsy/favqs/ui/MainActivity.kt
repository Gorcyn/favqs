package com.gorcyn.healsy.favqs.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.extensions.replaceFragment
import com.gorcyn.healsy.favqs.ui.login.LoginFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.findFragmentById(R.id.container) ?: LoginFragment.newInstance().also {
            replaceFragment(it, R.id.container)
        }
    }
}
