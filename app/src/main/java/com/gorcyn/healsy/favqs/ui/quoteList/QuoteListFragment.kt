package com.gorcyn.healsy.favqs.ui.quoteList

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

import com.jakewharton.rxbinding2.support.v4.widget.refreshes

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.extensions.snack
import com.gorcyn.healsy.favqs.util.ViewModelFactory

import kotlinx.android.synthetic.main.fragment_quote_list.quoteListProgressBar
import kotlinx.android.synthetic.main.fragment_quote_list.quoteListRecycler

class QuoteListFragment: Fragment() {

    companion object {
        private const val USER = "USER"
        const val TAG = "QuoteListFragment"

        fun newInstance(user: User): QuoteListFragment {
            val bundle = Bundle()
            bundle.putParcelable(USER, user)
            val fragment = QuoteListFragment()
            fragment.arguments = bundle
            return fragment

        }
    }

    private val disposables = CompositeDisposable()

    private val user: User by lazy {
        arguments!![USER] as User
    }

    private val viewModel: QuoteListViewModel by lazy {
        ViewModelProviders.of(this, ViewModelFactory.getInstance(context!!))
                .get(QuoteListViewModel::class.java)
    }

    private val adapter: QuoteListAdapter by lazy {
        QuoteListAdapter { viewModel.getNextList(user) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_quote_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        // Prepare recycler
        quoteListRecycler.adapter = adapter

        // Observe view model
        disposables += viewModel.isLoading
                .subscribe({ quoteListProgressBar.isRefreshing = it }, {})
        disposables += viewModel.quoteList
                .distinctUntilChanged()
                .subscribe({
                    adapter.quoteList = it
                    adapter.notifyDataSetChanged()
                }, {})
        disposables += viewModel.hasMore
                .distinctUntilChanged()
                .subscribe({
                    adapter.hasMore = it
                    adapter.notifyDataSetChanged()
                }, {})
        disposables += viewModel.error
                .subscribe({ snack(it.localizedMessage) }, {})

        // Observe user interactions
        disposables += quoteListProgressBar.refreshes().forEach { viewModel.getList(user, true) }

        // Load right now
        viewModel.getList(user)
    }

    override fun onPause() {
        super.onPause()
        disposables.dispose()
    }
}
