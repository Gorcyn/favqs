package com.gorcyn.healsy.favqs.data.source.local

import android.arch.persistence.room.TypeConverter

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object Converters {

    @JvmStatic
    @TypeConverter
    fun fromStringToListOfString(value: String?): List<String>? {
        val listType = object : TypeToken<ArrayList<String>>(){}.type
        return Gson().fromJson(value, listType)
    }

    @JvmStatic
    @TypeConverter
    fun toStringFromListOfString(list: List<String>?): String? {
        return Gson().toJson(list)
    }
}
