package com.gorcyn.healsy.favqs.data.source.exception

import kotlin.Exception

class WrongCredentialsException(
        message: String?,
        cause: Throwable? = null
): Exception(message, cause)
