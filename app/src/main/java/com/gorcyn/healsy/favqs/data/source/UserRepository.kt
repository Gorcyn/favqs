package com.gorcyn.healsy.favqs.data.source

import io.reactivex.Completable
import io.reactivex.Maybe

import com.gorcyn.healsy.favqs.data.User
import com.gorcyn.healsy.favqs.data.source.exception.WrongCredentialsException
import com.gorcyn.healsy.favqs.util.SingletonHolderDoubleArg

open class UserRepository(
        private val localDataSource: UserDataSource,
        private val remoteDataSource: UserDataSource
): UserDataSource {

    companion object : SingletonHolderDoubleArg<UserRepository, UserDataSource, UserDataSource>(::UserRepository)

    override fun login(login: String, password: String): Maybe<User> {
        return localDataSource.login(login, password)
                .switchIfEmpty(Maybe.defer {
                    remoteDataSource.login(login, password)
                            .map {
                                if (it.errorCode != null) {
                                    throw WrongCredentialsException("Wrong credentials")
                                } else  it
                            }
                            .doOnSuccess { localDataSource.save(it) }
                })
    }

    override fun getDetails(user: User): Maybe<User> {
        return remoteDataSource.getDetails(user)
                .map { it.copy(userToken = user.userToken) }
                .doOnSuccess { localDataSource.save(it) }
                .switchIfEmpty(Maybe.defer {
                    localDataSource.getDetails(user)
                })
    }

    override fun save(user: User): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
