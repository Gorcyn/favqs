package com.gorcyn.healsy.favqs.robot

import com.gorcyn.healsy.favqs.extensions.canSee
import com.gorcyn.healsy.favqs.extensions.click

fun profile(func: ProfileRobot.() -> Unit) {
    ProfileRobot().apply { func() }
}

class ProfileRobot {

    fun iSeeLogin(login: String) {
        login.canSee()
    }

    fun iSeeFavorites(favoritesCount: Int) {
        "$favoritesCount FAVORITES".canSee()
    }

    fun iClickOnFavorites(favoritesCount: Int) {
        "$favoritesCount FAVORITES".click()
    }
}
