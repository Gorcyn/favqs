package com.gorcyn.healsy.favqs.scenario

import org.junit.Test

import com.gorcyn.healsy.favqs.robot.login
import com.gorcyn.healsy.favqs.robot.profile
import com.gorcyn.healsy.favqs.robot.quoteList
import com.gorcyn.healsy.favqs.tests.UITest

class LoginThenGetToFavoriteQuotes: UITest() {

    @Test
    fun loginThenGetToFavoriteQuotes() {

        login {
            iLogin("gorcyn", "I7GHCZ9g&")
        }
        profile {
            iSeeLogin("gorcyn")
            iSeeFavorites(31)
            iClickOnFavorites(31)
        }
        quoteList {
            iSeeAQuote("Generally speaking, a howling wilderness does not howl: it is the imagination of the traveler that does the howling.")
        }
    }
}
