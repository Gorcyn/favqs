package com.gorcyn.healsy.favqs.data.source.local

import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import org.hamcrest.core.Is.`is`

import android.preference.PreferenceManager
import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4

import io.reactivex.observers.TestObserver

import com.gorcyn.healsy.favqs.data.User

@RunWith(AndroidJUnit4::class)
@LargeTest
class UserLocalDataSourceTest {

    private lateinit var localDataSource: UserLocalDataSource

    @Before
    fun setup() {
        UserLocalDataSource.clearInstance()

        localDataSource = UserLocalDataSource
                .getInstance(PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext()))
    }

    @After
    fun cleanUp() {
        localDataSource.delete()
    }

    @Test
    fun testPreConditions() {
        assertNotNull(localDataSource)
    }

    @Test
    fun saveUser_retrievesUser() {
        // Given a new user
        val newUser = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)

        // When saved into the persistent repository
        localDataSource.save(newUser)

        // Then the album can be retrieved from the persistent repository
        val testObserver = TestObserver<User>()
        localDataSource.getDetails(newUser).subscribe(testObserver)
        testObserver.assertValue(newUser)
    }

    @Test
    fun deleteUser_emptyRetrievedUser() {
        // Given a new user in the persistent repository and a mocked callback
        val newUser = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)
        localDataSource.save(newUser)

        // When user is deleted
        localDataSource.delete()

        // Then the retrieved user is empty
        val testObserver = TestObserver<User>()
        localDataSource.getDetails(newUser).subscribe(testObserver)
        val result = testObserver.values()
        assertThat(result.isEmpty(), `is`(true))
    }

    @Test
    fun getUser_whenUserNotSaved() {
        //Given that no user has been saved
        val newUser = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)

        //When querying for an album, null is returned.
        val testObserver = TestObserver<User>()
        localDataSource.getDetails(newUser).subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoValues()
    }
}
