package com.gorcyn.healsy.favqs.robot

import com.gorcyn.healsy.favqs.extensions.canSee

fun quoteList(func: QuoteListRobot.() -> Unit) {
    QuoteListRobot().apply { func() }
}

class QuoteListRobot {

    fun iSeeAQuote(quote: String) {
        quote.canSee()
    }
}
