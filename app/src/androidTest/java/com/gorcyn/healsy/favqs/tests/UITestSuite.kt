package com.gorcyn.healsy.favqs.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.healsy.favqs.data.source.local.QuoteLocalDataSourceTest
import com.gorcyn.healsy.favqs.data.source.local.UserLocalDataSourceTest
import com.gorcyn.healsy.favqs.scenario.LoginThenGetToFavoriteQuotes

@RunWith(Suite::class)

@Suite.SuiteClasses(
        QuoteLocalDataSourceTest::class,
        UserLocalDataSourceTest::class,
        LoginThenGetToFavoriteQuotes::class
)
class UITestSuite
