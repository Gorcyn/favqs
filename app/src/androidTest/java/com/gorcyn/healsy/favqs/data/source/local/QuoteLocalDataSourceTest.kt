package com.gorcyn.healsy.favqs.data.source.local

import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsCollectionContaining.hasItems

import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4

import io.reactivex.observers.TestObserver

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User

@RunWith(AndroidJUnit4::class)
@LargeTest
class QuoteLocalDataSourceTest {

    companion object {
        private val USER = User("LOGIN", "USER_TOKEN", "PIC_URL", 0)
    }

    private lateinit var localDataSource: QuoteLocalDataSource

    @Before
    fun setup() {
        QuoteLocalDataSource.clearInstance()

        localDataSource = QuoteLocalDataSource
                .getInstance(AppDatabase.getInstance(InstrumentationRegistry.getTargetContext()))
    }

    @After
    fun cleanUp() {
        localDataSource.deleteAll()
    }

    @Test
    fun testPreConditions() {
        assertNotNull(localDataSource)
    }

    @Test
    fun saveQuote_retrievesQuote() {
        // Given a new album
        val newUser = Quote(1, "AUTHOR", "BODY", listOf("TAG"))

        // When saved into the persistent repository
        localDataSource.save(newUser)

        // Then the album can be retrieved from the persistent repository
        val testObserver = TestObserver<List<Quote>>()
        localDataSource.getList(USER, 1).subscribe(testObserver)
        val result = testObserver.values()[0]
        assertThat(result.first(), `is`(newUser))
    }

    @Test
    fun deleteQuoteList_emptyListOfRetrievedQuote() {
        // Given a new album in the persistent repository and a mocked callback
        val newQuote = Quote(1, "AUTHOR", "BODY", listOf("TAG"))
        localDataSource.save(newQuote)

        // When all quotes are deleted
        localDataSource.deleteAll()

        // Then the retrieved quotes is an empty list
        val testObserver = TestObserver<List<Quote>>()
        localDataSource.getList(USER, 1).subscribe(testObserver)
        val result = testObserver.values()[0]
        assertThat(result.isEmpty(), `is`(true))
    }

    @Test
    fun getQuoteList_retrieveSavedQuoteList() {
        // Given 2 new quotes in the persistent repository
        val newQuote1 = Quote(1, "AUTHOR", "BODY", listOf("TAG"))
        localDataSource.save(newQuote1)
        val newQuote2 = Quote(2, "AUTHOR", "BODY", listOf("TAG"))
        localDataSource.save(newQuote2)

        // Then the quotes can be retrieved from the persistent repository
        val testObserver = TestObserver<List<Quote>>()
        localDataSource.getList(USER, 1).subscribe(testObserver)
        val result = testObserver.values()[0]
        assertThat(result, hasItems(newQuote1, newQuote2))
    }
}
