package com.gorcyn.healsy.favqs.robot

import com.gorcyn.healsy.favqs.R
import com.gorcyn.healsy.favqs.extensions.click
import com.gorcyn.healsy.favqs.extensions.type

fun login(func: LoginRobot.() -> Unit) {
    LoginRobot().apply { func() }
}

class LoginRobot {

    fun iLogin(login: String, password: String) {
        R.id.loginLoginField.type(login)
        R.id.loginPasswordField.type(password)
        R.id.loginButton.click()
    }
}
