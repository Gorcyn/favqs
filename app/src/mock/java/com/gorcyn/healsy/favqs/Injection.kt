package com.gorcyn.healsy.favqs

import android.content.Context

import com.gorcyn.healsy.favqs.data.source.QuoteDataSource
import com.gorcyn.healsy.favqs.data.source.UserDataSource
import com.gorcyn.healsy.favqs.data.source.FakeQuoteDataSource
import com.gorcyn.healsy.favqs.data.source.FakeUserDataSource
import com.gorcyn.healsy.favqs.util.schedulers.BaseSchedulerProvider
import com.gorcyn.healsy.favqs.util.schedulers.SchedulerProvider

class Injection {

    companion object {
        fun provideUserDataSource(context: Context): UserDataSource {
            return FakeUserDataSource
        }

        fun provideQuoteDataSource(context: Context): QuoteDataSource {
            return FakeQuoteDataSource
        }

        fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider
    }
}
