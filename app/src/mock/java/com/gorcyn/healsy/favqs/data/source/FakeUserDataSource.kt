package com.gorcyn.healsy.favqs.data.source

import java.util.concurrent.TimeUnit

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers

import com.gorcyn.healsy.favqs.data.User

object FakeUserDataSource: UserDataSource {
    private val USER = User("gorcyn", "TOKEN", "PICK_URL", 0)

    override fun login(login: String, password: String): Maybe<User> {
        return Maybe.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { USER }
    }

    override fun getDetails(user: User): Maybe<User> {
        return Maybe.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { USER }
    }

    override fun save(user: User): Completable {
        return Completable.complete()
    }
}
