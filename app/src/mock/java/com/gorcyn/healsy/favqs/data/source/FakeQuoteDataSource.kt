package com.gorcyn.healsy.favqs.data.source

import java.util.concurrent.TimeUnit

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers

import com.gorcyn.healsy.favqs.data.Quote
import com.gorcyn.healsy.favqs.data.User

object FakeQuoteDataSource: QuoteDataSource {
    private val QUOTE_LIST = listOf(
            Quote(1, "AUTHOR 1", "BODY 1", listOf("TAG 1.1", "TAG 1.2")),
            Quote(2, "AUTHOR 2", "BODY 2", listOf("TAG 2.1", "TAG 2.2")),
            Quote(3, "AUTHOR 3", "BODY 3", listOf("TAG 3.1", "TAG 3.2"))
    )

    override fun getList(user: User, page: Int): Single<List<Quote>> {
        return Single.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { QUOTE_LIST }
    }

    override fun save(quote: Quote): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun refreshList(clear: Boolean) {

    }
}
